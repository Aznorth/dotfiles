dotfiles
========

Personal configuration for

- abcde
- alacritty
- bash
- gammastep
- git
- gtk
- mako
- mpd + ncmpcpp
- sway
- vim
- waybar

and maybe other things, managed with GNU Stow (using --target=${HOME})

_Note_ : You will have to install the LanguageTool jarmanually. Or you can comment it out.
See [LanguageTool on vim online](http://www.vim.org/scripts/script.php?script_id=3223)

[More about GNU Stow](http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html)

## /etc/issue

put this inside :

```
 \e[H\e[2J
           \e[0;36m.
          \e[0;36m/ \\       \e[1;37m     _             _         
         \e[0;36m/   \\      \e[1;37m    / \\   _ __ ___| |__    \e[1;36m| *
        \e[0;36m/^.   \\     \e[1;37m   / _ \\ | '__/ __| '_ \\   \e[1;36m| | |-^-. |   | \\ /
       \e[0;36m/  .-.  \\    \e[1;37m  / ___ \\| | | (__| | | \\  \e[1;36m| | |   | |   |  X
      \e[0;36m/  (   ) _\\   \e[1;37m /_/   \\_\\_|  \\___|_| |_|  \e[1;36m| | |   | ^._.| / \\ \e[0;37mTM
     \e[1;36m/ _.~   ~._^\\
    \e[1;36m/.^         ^.\\ \e[0;37mTM


Arch Linux \r (\l)
Welcome to \n
\d \t

```

