#
# ~/.bash_profile
#

PATH=~/bin:$PATH

[[ -f ~/.bashrc ]] && . ~/.bashrc

# Wayland with Sway
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
    export QT_QPA_PLATFORMTHEME="qt5ct"
    export MOZ_ENABLE_WAYLAND=1
    export BEMENU_OPTS='--hb=#285577 --hf=#ffffff --tb=#285577 --tf=#ffffff'
    exec sway
fi

