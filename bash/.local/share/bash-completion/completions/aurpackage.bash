#/usr/bin/env bash
_aurpackage_completions()
{
  if [ "${#COMP_WORDS[@]}" != "2" ]; then
    return
  fi

  COMPREPLY=($(compgen -W "list install check help" -- "${COMP_WORDS[1]}"))
}

complete -F _aurpackage_completions aurpackage
