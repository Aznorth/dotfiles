alias ls='ls -F --color=auto'
alias ll='ls -lF --color=auto'
alias lll='ls -AlrtFqsh --color=auto'
#alias tunnel='ssh -p 443 -D 2080 <user>@<IP>'
alias fwlog='journalctl -k | grep "IN=.*OUT=.*" | less'
alias sshenv='eval $(ssh-agent) && ssh-add ~/.ssh/id_rsa'
alias orphelins='echo "pacman -Qdt" && pacman -Qdt'
alias notes="$EDITOR ~/MesDocs/Textes/_notes_en_vrac.txt"

stopwatch(){
    date1=`date +%s`;
    while true; do
        days=$(( $(($(date +%s) - date1)) / 86400 ))
        echo -ne "$days day(s) and $(date -u --date @$((`date +%s` - $date1)) +%H:%M:%S)\r";
        sleep 0.1
    done
}

